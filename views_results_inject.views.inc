<?php
/**
 * @file
 * Override entity row plugin handler.
 */

/**
 * Implements hook_views_plugins().
 */
function views_results_inject_views_plugins() {
  $entity_plugin = entity_views_plugins();
  // Copying entity plugin settings into entity_inject_content.
  $entity_plugin['row']['entity_inject_content'] = $entity_plugin['row']['entity'];
  unset($entity_plugin['row']['entity']);
  $entity_plugin['row']['entity_inject_content']['handler'] = 'ViewsResultsInjectPlugin';
  $entity_plugin['row']['entity_inject_content']['title'] = t('Rendered entity with injected custom markup');

  return $entity_plugin;
}
