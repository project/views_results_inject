Module: Views Results Inject

Description
===========
This module extends the 'Rendered entity' views row plugin allowing
developers to inject custom markup between results.

Installation
============
 - Put the modules into your sites/modules folder
 - Enable the module

Configuration
=============
 - On desired views which are using 'Rendered Entity' as a row plugin, select
   'Rendered entity with injected custom markup'. Keep 'View Mode' setting
   the same as was on 'Rendered Entity'
 - Implement hook_views_post_execute() in one of your modules where you inject
   the content into results
 - Create a callback function

Example
=======
 /**
   * Implements hook_views_post_execute().
   */
  function yourmodule_views_post_execute(&$view) {
    if ($view->name == 'yourview' && $view->current_display == 'somedisplay') {
      // Inject output of yourmodule_custom_callback into '2' position of your results set.
      $view->result = views_results_inject_content($view, 2, 'yourmodule_custom_callback', array($view->name));
    }
  }

 /**
   * Custom callback function.
   */
  function yourmodule_custom_callback($name) {
    return t("Embed custom markup into results of the view @view", array('@view' => $name));
  }
