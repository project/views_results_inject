<?php
/**
 * @file
 * Override for the entity row style plugin.
 *
 * Row style plugin for displaying the results as entities with an ability
 * to insert a custom content in between.
 */

/**
 * Views row plugin handler.
 */
class ViewsResultsInjectPlugin extends entity_views_plugin_row_entity_view {
  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if ($this->custom_callback_exists($values)) {
      return call_user_func_array($values->entity_inject_content['callback'], $values->entity_inject_content['params']);
    }
    elseif ($entity = $this->get_value($values)) {
      // Add the view object as views_plugin_row_node_view::render() would.
      // Otherwise the views theme suggestions won't work properly.
      $entity->view = $this->view;
      $render = $this->rendered_content[entity_id($this->entity_type, $entity)];
      return drupal_render($render);
    }
  }

  /**
   * Determines if callback function exists.
   *
   * @param object $values
   *   Individual views row object.
   *
   * @return bool
   *   True if callback exists. False otherwise.
   */
  protected function custom_callback_exists($values) {
    return !empty($values->entity_inject_content) && function_exists($values->entity_inject_content['callback']) ? TRUE : FALSE;
  }

}
